from setuptools import setup
from setuptools import find_packages

setup(
    name='kvdbpy',
    version='1.0.0',
    description='Python implementation of KV db. Keys are stored in-memory hash table.',
    author='Vinayak Pahalwan',
    url='https://gitlab.com/pahalwan.vinayak/kvdbpy',
    packages=find_packages(exclude=('tests*'))
)