KV store implementation, not for production

Usage

```
from kvdb import Kvdb
db = Kvdb("exampledb")
db.put("message", "Hello")
print(db.get("message"))
```
